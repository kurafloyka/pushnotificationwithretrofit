package com.example.pushnotification.Kayit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pushnotification.Model.Result;
import com.example.pushnotification.R;
import com.example.pushnotification.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    EditText kadi, mail, sifre;
    Button kayitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        define();
        action();
    }


    public void define() {

        kadi = findViewById(R.id.kadi);
        mail = findViewById(R.id.mail);
        sifre = findViewById(R.id.sifre);
        kayitButton = findViewById(R.id.kayit);

    }

    public void request() {

        String kadiText = kadi.getText().toString();
        final String mailText = mail.getText().toString();
        String sifreText = sifre.getText().toString();

        Log.i("alanlar", kadiText + mailText + sifreText);
        Call<Result> kayitekle = ManagerAll.getInstance().addUserResultCall(kadiText, sifreText, mailText);


        kayitekle.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    Log.i("response : ", response.body().getResult());
                    Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();
                    String message = response.body().getResult();
                    if (message.contains("Bu mail ile kayit basari ile eklenmistir.")) {

                        Log.i("response", "Geldim");
                        Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
                        intent.putExtra("mail", mailText);
                        startActivity(intent);


                    } else {
                        Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.i("response : ", t.toString());
            }
        });
    }

    public void action() {

        kayitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request();
            }
        });
    }
}
