package com.example.pushnotification.Kayit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pushnotification.Model.Result;
import com.example.pushnotification.R;
import com.example.pushnotification.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main3Activity extends AppCompatActivity {

    EditText mail, kod;
    Button kodGonder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        define();

        Bundle bundle = getIntent().getExtras();
        String mailBilgisi = bundle.getString("mail");

        //Log.i("mail",mailBilgisi);
        mail.setText(mailBilgisi);
        action();
    }


    public void define() {


        mail = findViewById(R.id.mail);
        kod = findViewById(R.id.kod);
        kodGonder = findViewById(R.id.kodGonder);


    }

    public void action() {


        kodGonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                request();
            }
        });

    }

    public void request() {

        //Log.i("request", "requestici");
        String kodText = kod.getText().toString();
        String mailText = mail.getText().toString();


        Call<Result> registerTool = ManagerAll.getInstance().registerUserCall(kodText, mailText);
        registerTool.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

               // Log.i("mesaj", "Burada");
                if (response.isSuccessful()) {

                    Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.i("response : ", t.toString());
            }
        });


    }
}
