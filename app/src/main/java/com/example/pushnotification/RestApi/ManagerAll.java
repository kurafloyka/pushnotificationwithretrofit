package com.example.pushnotification.RestApi;


import com.example.pushnotification.Model.Result;

import java.util.List;

import retrofit2.Call;

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }


    public Call<Result> addUserResultCall(String kadi, String sifre, String mail) {
        Call<Result> addUserResult = getRestApiClient().addUser(kadi, sifre, mail);
        return addUserResult;
    }

    public Call<Result> registerUserCall(String kod, String mail) {

        Call<Result> registerUser = getRestApiClient().register(kod, mail);
        return registerUser;
    }
}
