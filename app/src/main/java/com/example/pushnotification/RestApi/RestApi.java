package com.example.pushnotification.RestApi;


import com.example.pushnotification.Model.Result;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestApi {
    @FormUrlEncoded
    @POST("kayitol.php")
    Call<Result> addUser(@Field("kadi") String kadi, @Field("sifre") String sifre, @Field("mail") String mail);

    @FormUrlEncoded
    @POST("kayitolaktif.php")
    Call<Result> register(@Field("kod") String kod, @Field("mail") String mail);
}




