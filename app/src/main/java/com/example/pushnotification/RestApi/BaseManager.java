package com.example.pushnotification.RestApi;

public class BaseManager {


    protected RestApi getRestApiClient() {

        RestApiClient restApiClient = new RestApiClient(BaseUrl.postUrl);

        return restApiClient.getRestApi();
    }
}
